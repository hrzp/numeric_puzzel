var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {

    var win = [['0', '1', '2'],
               ['3', '4', '5'],
               ['6', '7', '8'],];

    var puzzel_borad = [['1', '1', '1'],
                        ['1', '1', '1'],
                        ['1', '1', '0'],];

    var puzzel_node = [['0', '1', '2'],
                       ['3', '4', '5'],
                       ['6', '7', '8'],];

    var init = function(){
        $scope.node.init();
    };


    $scope.node = (function(){
        var self = {};


        self.move = function(event, pos){
            var x_y = self.find_pos(pos);
            var dir = self.posible_to_move(x_y);
            if (dir != 'block'){
                self.move_to(event, dir);
            }
            if (self.is_win()){
                alert('ok you win');
                self.init();
            }
        };


        self.init = function(){
            var x = document.getElementsByClassName("btn");
            // console.log(x[0].offsetLeft,x[0].offsetTop);
            // for (var i = x.length - 1; i >= 0; i--){
            //     x[i].style.left = '15px';
            //     x[i].style.top = '0px';
            // }
            x[1].style.left = String(x[1].offsetLeft + 105) + 'px';
            x[2].style.left = String(x[2].offsetLeft + 210) + 'px';

            x[3].style.top = String(x[3].offsetTop + 105) + 'px';
            x[4].style.top = String(x[4].offsetTop + 105) + 'px';
            x[4].style.left = String(x[4].offsetLeft + 105) + 'px';
            x[5].style.top = String(x[5].offsetTop + 105) + 'px';
            x[5].style.left = String(x[5].offsetLeft + 210) + 'px';

            x[6].style.top = String(x[6].offsetTop + 210) + 'px';
            x[7].style.top = String(x[7].offsetTop + 210) + 'px';
            x[7].style.left = String(x[7].offsetLeft + 105) + 'px';
            var i = 0;
            var sleep = setInterval(function(){
                i++;
                var j = Math.floor((Math.random() * 16) / 2);
                var dir = 'block';
                var last_j;
                while(dir == 'block'){
                    var x_y = self.find_pos(String(j));
                    dir = self.posible_to_move(x_y);
                    if (dir != 'block'){
                        self.move_rnd(x[j], dir);
                        j = Math.floor((Math.random() * 16) /2);
                    }
                    else{
                        j = Math.floor((Math.random() * 16) /2);
                    }
                }
                // if(self.is_win()){
                //     clearInterval(sleep);
                //     alert('you win');
                //     console.log(i);
                // }
                if(i>=25){
                    clearInterval(sleep);
                }
            }, 500);
        };


        self.move_to = function(obj, dire){
            switch (dire) {
                case 'up':
                    obj.currentTarget.style.top = String(obj.currentTarget.offsetTop - 105) + 'px';
                    break;
                case 'down':
                    obj.currentTarget.style.top = String(obj.currentTarget.offsetTop + 105) + 'px';
                    break;
                case 'left':
                    obj.currentTarget.style.left = String(obj.currentTarget.offsetLeft - 105) + 'px';
                    break;
                case 'right':
                    obj.currentTarget.style.left = String(obj.currentTarget.offsetLeft + 105) + 'px';
                    break;
            }
            return obj;
        };


        self.is_win = function(){
            for (var i = win.length - 1; i >= 0; i--) {
                for (var j = puzzel_node.length - 1; j >= 0; j--) {
                    if(puzzel_node[i][j] != win[i][j]){
                        return false;
                    }
                }
            }
            return true;
        }


        self.move_rnd = function(obj, dire){
            switch (dire) {
                case 'up':
                    obj.style.top = String(obj.offsetTop - 105) + 'px';
                    break;
                case 'down':
                    obj.style.top = String(obj.offsetTop + 105) + 'px';
                    break;
                case 'left':
                    obj.style.left = String(obj.offsetLeft - 105) + 'px';
                    break;
                case 'right':
                    obj.style.left = String(obj.offsetLeft + 105) + 'px';
                    break;
            }
            return obj;
        };


        self.find_pos = function(pos){
            var i, j;
            for (i = 0; i < 3; i++){
                for (j = 0; j < 3; j++){
                    if (puzzel_node[i][j] == pos){
                        return {'x': i, 'y': j};
                    }
                }
            }
            return false;
        };


        self.posible_to_move = function(pos){
            var x = Number(pos.x);
            var y = Number(pos.y);
            var dire = 'block';
            if (self.is_down_free(x, y)){
                dire = 'down';
            }
            else if (self.is_up_free(x, y)){
                dire = 'up';
            }
            else if (self.is_left_free(x, y)){
                dire = 'left';
            }
            else if (self.is_right_free(x, y)){
                dire = 'right';
            }
            self.move_nodes(x, y, dire);
            return dire;
        };


        self.is_down_free = function(x, y){
            if ((x + 1) < 3){
                if (puzzel_borad[x + 1][y] == '0'){
                    dire = 'down';
                    return true;
                }
            }
            return false;
        };


        self.is_up_free = function(x, y){
            if ((x - 1) > -1){
                if (puzzel_borad[x - 1][y] == '0'){
                    dire = 'down';
                    return true;
                }
            }
            return false;
        };


        self.is_left_free = function(x, y){
            if ((y - 1) > -1){
                if (puzzel_borad[x][y - 1] == '0'){
                    dire = 'left';
                    return true;
                }
            }
            return false;
        };


        self.is_right_free = function(x, y){
            if ((y + 1) < 3){
                if (puzzel_borad[x][y + 1] == '0'){
                    dire = 'right';
                    return true;
                }
            }
            return false;
        };


        self.move_nodes = function(x, y, dir){
            if (dir == 'down'){
                puzzel_borad[x + 1][y] = '1';
                puzzel_borad[x][y] = '0';
                var tmp = puzzel_node[x][y];
                puzzel_node[x][y] = puzzel_node[x + 1][y];
                puzzel_node[x + 1][y] = tmp;
            }
            else if(dir == 'up'){
                puzzel_borad[x - 1][y] = '1';
                puzzel_borad[x][y] = '0';
                var tmp = puzzel_node[x][y];
                puzzel_node[x][y] = puzzel_node[x - 1][y];
                puzzel_node[x - 1][y] = tmp;
            }
            else if(dir == 'left'){
                puzzel_borad[x][y - 1] = '1';
                puzzel_borad[x][y] = '0';
                var tmp = puzzel_node[x][y];
                puzzel_node[x][y] = puzzel_node[x][y - 1];
                puzzel_node[x][y - 1] = tmp;
            }
            else if(dir == 'right'){
                puzzel_borad[x][y + 1] = '1';
                puzzel_borad[x][y] = '0';
                var tmp = puzzel_node[x][y];
                puzzel_node[x][y] = puzzel_node[x][y + 1];
                puzzel_node[x][y + 1] = tmp;
            }
        };

        return self;
    })();


    init();


});